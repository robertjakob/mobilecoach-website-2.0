module.exports = {
  title: 'MobileCoach',
  tagline: 'The Open Source Digital Intervention Platform',
  url: 'https://mobile-coach.eu',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.ico',
  organizationName: 'c4dhi-org', // Usually your GitHub org/user name.
  projectName: 'MobileCoach Website', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'MobileCoach',
      logo: {
        alt: 'MobileCoach',
        src: 'img/mc_icon.png',
      },
      items: [
        {
          to: 'docs/about/what-is-mc/',
          activeBasePath: 'docs/about/what-is-mc',
          label: 'About',
          position: 'left',
        },
        {
          to: 'docs/installation/server/setup/',
          activeBasePath: 'docs/installation',
          label: 'Setup',
          position: 'left',
        },
        {
          to: 'docs/tutorials/webdesigner/overview/',
          activeBasePath: 'docs/tutorials',
          label: 'Tutorials',
          position: 'left',
        },
        {
          to: 'docs/about/publications/',
          activeBasePath: 'docs/about/publications',
          label: 'Publications',
          position: 'left',
        },
        {
          to: 'docs/about/projects/',
          activeBasePath: 'docs/about/projects',
          label: 'Projects',
          position: 'left',
        },
        { to: 'blog', label: 'Blog', position: 'left' },
        {
          to: 'docs/about/repositories/',
          activeBasePath: 'docs/about/repositories',
          label: 'Repositories',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Setup',
              to: 'docs/installation/server/setup/',
            },
            {
              label: 'Tutorials',
              to: 'docs/tutorials/webdesigner/overview/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'MobileCoach User Forum',
              href: 'https://discourse-mobilecoach.ch/',
            },
            {
              label: 'Center for Digital Health Interventions',
              href: 'https://www.c4dhi.org/',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'Contact',
              to: 'docs/about/contact/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Center for Digital Health Interventions @ ETH Zurich & University of St. Gallen`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          versions: {
            current: {
              label: 'current'
            },
          },
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://bitbucket.org/mobilecoach/mobilecoach-website-2.0/src/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [require.resolve('@docusaurus/plugin-ideal-image')],
};
