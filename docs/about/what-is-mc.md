---
id: what-is-mc
title: What is MobileCoach
---

The vision of the MobileCoach platform is to create smart fully-automated digital interventions.

Due to a modular and extendable structure as well as an architecture rooted in the logics and legal claims of open-source software, the MobileCoach lays a fruitful ground for digital interventions in several application domains of behavior change. For example, the MobileCoach can be used to design and evaluate digital health interventions in the context of smoking cessation, nutrition and physical activity and many more.

Based on personal characteristics and prior behavior assessed when participating in a digital intervention, the MobileCoach uses communication services or sensor services (e.g. physical activity tracker) to monitor central indicators of the particular behavior on a regular basis. Building on individual data, MobileCoach users profit from individualized messages and valuable insights motivating for long-term behavior change.

## Infrastructure of MobileCoach

MobileCoach exists of three following main parts.

### MobileCoach Web Designer

The Web Designer is the main infrastructure for your intervention. We developed a very powerful interface in which you can create your digital intervention. This includes: variables, automatically executed rules, micro-dialogs, push-notifications, sms and many more. Find more details [HERE](../tutorials/webdesigner/overview.md).

![Web Designer](/img/webdesigner.png 'Web Designer')

### MobileCoach Mobile App

The Mobile App is written in React Native and can easily be deployed for Android and iOS. The main feature is the multifunctional chat, in which the participant can react with the Chatbot. It's also possible to have a second chat channel through the cockpit, this is mostly used for study supervisors or similar.

![Mobile App](/img/mobileapp.png 'Mobile App')

### MobileCoach Web Cockpit

The Web Cockpit is an additional interface to monitor the participants of the intervention. You can also conduct a study/intervention without the Web Cockpit. The Cockpit is mostly used by additional health professionals, researcher or general study supporter. The Web Cockpit is currently being revised and is not yet publicly accessible. If you still want to use it, please contact us.

![Web Cockpit](/img/cockpit.png 'Web Cockpit')

## Awards

MobileCoach received the following awards:

* The mobile MAX intervention for children with Asthma entitled “MAX – Dein Asthmacoach” received the Intermedia-globe **Grand Award 2019** in the category Web at the World Media Festival 2019 in Hamburg, Germany.
* The mobile MAX intervention for children with Asthma entitled “MAX – Dein Asthmacoach” received the Intermedia-globe **Gold Award 2019** in the category Mobile Apps & Interactive Tools at the World Media Festival 2019 in Hamburg, Germany.
* **Impact Award 2018**, University of St.Gallen, St. Gallen, Switzerland [Click here for the video](https://www.youtube.com/watch?v=Z9_WGpe5lP4&feature=emb_title)
* **Outstanding Paper Award** at the IEEE Wireless Telecommunications Symposium 2015 (WTS 2015), New York, USA.
* **Best Graduate Student Paper Award** at the IEEE Wireless Telecommunications Symposium 2015 (WTS 2015), New York, USA.
