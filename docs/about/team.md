---
id: team
title: Team
---

The MobileCoach is continuously improved by an interdisciplinary research team. Among the various disciplines, team members are particularly experts in computer science, psychology and the field of public health. Please notice that there are many individuals working on the MobileCoach system or MobileCoach interventions.

## Centre for Digital Health Interventions, ETH Zurich & University of St.Gallen, Switzerland

### Prof. Dr. Tobias Kowatsch

Initiator and System Architect of MobileCoach; Scientific Director, [Centre for Digital Health Interventions](https://www.c4dhi.org), ETH Zurich & University of St.Gallen, Switzerland; Lead Principal Investigator, [Centre for Digital Health InterventionsMobile Health Interventions](https://fht.ethz.ch), [Singapore-ETH Centre](https://sec.ethz.ch), Singapore; Partner, [Center for Technology and Behavioral Health](https://www.c4tbh.org), Dartmouth College, USA; Assistant Professor for Digital Health, University of St.Gallen, St.Gallen, Switzerland; Doctor of Philosophy in Management (Ph.D.), MSc in Business Informatics, MSc in Computer Science in Media, and Dipl.-Inform. (FH) in Computer Science in Media, <tkowatsch@ethz.ch>

### Prabhakaran 'Prabhu' Santhanam

MobileCoach Software Engineer and Open Source Community Manager at the [Centre for Digital Health Interventions](https://www.c4dhi.org), ETH Zurich and University of St.Gallen, Switzerland; MSc in Computer Science, <psanthanam@ethz.ch>

### Fabian Schneider

MobileCoach Software Engineer and Open Source Community Manager at the [Centre for Digital Health Interventions](https://www.c4dhi.org), ETH Zurich and University of St.Gallen, Switzerland; BSc in Computer Science, <fschneider@ethz.ch>


### Prof. Dr. Florian von Wangenheim

Advisory Board Member of the [Centre for Digital Health Interventions](https://www.c4dhi.org), ETH Zurich and University of St.Gallen, Switzerland; Chair of Information Management at the Department of Management, Technology and Economics, ETH Zurich, and Chair of Operations Management at the Institute of Technology Management, University of St. Gallen, <fwangenheim@ethz.ch>

### Prof. Dr. Elgar Fleisch

Advisory Board Member of the [Centre for Digital Health Interventions](https://www.c4dhi.org), ETH Zurich and University of St.Gallen, Switzerland; Chair of Information Management at the Department of Management, Technology and Economics, ETH Zurich, and Chair of Operations Management at the Institute of Technology Management, University of St. Gallen, <efleisch@ethz.ch>


## ZHAW School of Life Sciences and Facility Management, Switzerland

### Petra Irène Lustenberger

Research Associate, Senior Software Engineer, Project Leader, Knowledge Engineering at ZHAW, Wädenswil, Switzerland, <petra.lustenberger@zhaw.ch>

### Davide Stallone

Software Engineer at ZHAW, Wädenswil, Switzerland, <davide.stallone@zhaw.ch>

### Dr. Robert Vorburger

Head of Research on Knowledge Engineering and the Data Science Laboratory (ZHAW Datalab) at ZHAW, Wädenswil, Switzerland, <robert.vorburger@zhaw.ch>

## Swiss Research Institute for Public Health and Addiction, University of Zurich, Switzerland

### PD Dr. Severin Haug

Initiator of MobileCoach; Psychologist and Head of Research at the Swiss Research Institute for Public Health and Addiction, <severin.haug@isgf.uzh.ch>

### Raquel Paz Castro

Psychologist at the Swiss Research Institute for Public Health and Addiction; M.Sc., Psychologist, <raquel.paz@isgf.uzh.ch>

## Former Developers & Open Source Contributors

### Sascha Gfeller

MobileCoach Software Engineer and Open Source Community Manager at the Center for Digital Health Interventions, BSc in Medical Informatics

### Andreas Filler

Co-Founder [Pathmate Technologies](https://www.pathmate-technologies.com), M.Sc. in Computer Science in Media & Dipl.-Inform. (FH) in Computer Science in Media, <filler@pathmate-technologies.com>

### Dr. Dirk Volland

Co-Founder [Pathmate Technologies](https://www.pathmate-technologies.com); Doctor of Philosophy in Management (Ph.D.), Dipl.-Winf (M.Sc.) in Business Informatics, <volland@pathmate-technologies.com>

### Jonas Fockstedt & Ema Krcic

Google-Fit integration for retrieving step data