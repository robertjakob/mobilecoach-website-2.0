---
id: contact
title: Contact
---

Please let us know if you have any further questions or comments regarding the **MobileCoach** platform. We are eager to learn from your experience to improve the MobileCoach.

* For general **research activities** or **collaboration requests** related to the MobileCoach, please contact **Tobias Kowatsch** <tkowatsch@ethz.ch>.

* For details on **addiction-related interventions or collaborations**, please contact our domain expert **Severin Haug** <severin.haug@isgf.uzh.ch>.

* For requests related to **professional product development of clinical pathways / medical device software**, please contact **Pathmate Technologies**. ([Website](https://www.pathmate-technologies.com/en/index))
