---
id: setup
title: Setting up the server
---

## **Requirements**

***
Server:

* Linux server with hostname [HOSTNAME]
* Ports 80, 443, [PORT_TOMCAT] and [PORT_DEEPSTREAM] open
* Docker installed (version 18.0.3 +)
* Certbot installed (for SSL certificates)
* Docker-compose installed v1.3+
* Openssl and openjdk installed
* Git installed
* Sudo access

## **Configuration**

***

### Connect to the server

* Open your terminal (Mac) / CMD (Windows)

```
ssh [USERNAME]@[HOSTNAME]
```

For example:

```
ssh sgfeller@workshop-cdhi.ethz.ch
```

Optional but recommended:
Create a new folder in the root directory where you afterwards pull the config.
For example:

```
cd /.
sudo mkdir /opt/
cd opt
```

### Pulling the configs

Pull the configuration and move into the *mobilecoach-server* folder.  
The rest of the commands in the following sections need to be run in folder *mobilecoach-server*

```
sudo bash
git clone https://bitbucket.org/mobilecoach/mobilecoach-server.git
cd mobilecoach-server
```

### Install letsencrypt certificates & updating config files

In order to generate the certificates and perform the configuration, a script must first be executed.

```
chmod +x ./setup-server.sh
./setup-server.sh
```

At the start, four details are required:  
Hostname, Tomcat Port, Deepstream Port and an email address needed for the letsencrypt notifications.  
It looks like this:

```
Please enter the hostname of the server (example: workshop-cdhi.ethz.ch)
Please enter the tomcat port (example: 8443)
Please enter the deepstream port (example: 6020)
Please enter your email id for letsencrypt notices (example: abcd@ef.gh)
```

The entered information is displayed again for verification.  
If all entries are correct, a "yes" must be entered.

```
The information provided, hostname: workshop-cdhi.ethz.ch, tomcat port: 8443, deepstream port: 6020 and email: sgfeller@ethz.ch
Are you sure of these details, and used this script with 'sudo' and want to continue setup? If okay, please answer with 'yes'
```

A password is generated that must be entered twice.

```
Please copy and use *8fbff0f0e4e636a2* as export password below.
Enter Export Password:
Verifying - Enter Export Password:
```

### Modifying the MobileCoach configuration file for different purposes. (Optional)

All other variables are explained [HERE](../server/configs.md)

### Starting the server

Starting the server for the first time

```
docker-compose up -d
```

Stopping, starting & restarting

```
docker-compose stop
docker-compose start
docker-compose restart
```

Stopping and removing the containers

```
docker-compose down
```

You can access the MC webApp at 'https://[HOSTNAME]/MC/admin'. Eg. https://workshop-cdhi.ethz.ch/MC/admin

:::note
The default admin username and password is *admin* and *admin* respectively. Please change it immediately under "Account".
:::

### Renewing the SSL certificates

Make sure the script renew.sh is runnable. Run the following command.

```
chmod +x ./renew.sh
```

For renewal, just run the following command.

```
./renew.sh
```

> **Note:** If you get asked about "Eintragsalias tomcat ist bereits vorhanden. Überschreiben? [Nein]" you can answer with "Ja".

## **Troubleshooting**

***

* to be displayed

## **Advanced user options**

***

* to be displayed
