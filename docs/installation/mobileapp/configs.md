---
id: configs
title: Configuration
---

## **Introduction**

This manual deals with the MC Client configuration file and its variables.  
You can find the file in the following path: ```/App/Config/AppConfig.js```.  
The basic configuration of the client is shown below and all variables are explained briefly.

## **Configuration**

### General

**allowTextFontScaling**  
Font Scaling override

Default value: *true*

**project**  
Current instantiation of app for project. Should be adjusted in camelCase name of app, e.g., demoXyz.  
Hint: Must be adjusted for encrypted apps!

Default value: *mobileCoachClient*

**projectName**  
Project name as it can be presented to the customer

Default value: *MobileCoach Client App*

**projectSecret**  
Encryption secret for instance of app project.

Default value: *mc-client-top-secret*

**supportedLanguages**
Supported languages in the client

Default value: *de, fr, it
___

### Development (dev)

**purgeStoreAtStartup**  
When it's true, everytime when you open the app, everything is purged and the onboarding is happening.

Default value: *false*

**fakeDeviceAlwaysOnlineForOfflineDev**  
Description

Default value: *false*

**fakeQRCodeScanWithURL**  
Description

Default value: *null*

**allowDebugKeyboard**  
Description

Default value: *false*

**deepstreamUserForDebugging**  
Description

Default value: *null*

**deepstreamSecretForDebugging**  
Description

Default value: *null*

**disableYellowbox**  
Description

Default value: *true*
___

### Storage

**encryptedReduxStorage**  
Description

Default value: *false*

**reduxStorageBlacklist**  
Description

Default value:

* search
* nav
* hydrationCompleted
* serverSyncStatus
* giftedchatmessages
* guistate

___

### Logger

Levels: 'DEBUG', 'INFO', 'WARN', 'ERROR', 'OFF', 'CRASHLYTICS'

**defaultLevel**  
'OFF' to deactivate the WHOLE logger (also exceptions)

Default value: *DEBUG*

**trackActivities**  
Description

Default value: *false*

**trackingURL**  
Description

Default value: *https://---/piwik/piwik.php*

**trackingId**  
Description

Default value: *5*

**loggerLevels**  
Description

Default values:

```javascript
{
    'AddMealModule/AddMealPreStep': 'INFO',
    'AddMealModule/AddFoodStep': 'INFO',
    'AddMealModule/AddMealContainer': 'INFO',
    'AddMealModule/SelectableFoodList': 'INFO',
    'AddMealModule/FoodMetrics': 'INFO',
    'Components/CameraComponent': 'INFO',
    'Components/RecordAudioComponent': 'INFO',
    'Components/CustomMessages/MediaInput': 'INFO',
    'Containers/AddMealModule/FoodMetrics': 'WARN',
    'FoodDiary/DiaryView': 'INFO',
    'Navigation/ReduxNavigation': 'INFO',
    'Redux/MessageRedux': 'INFO',
    'Redux/ServerSyncRedux': 'INFO',
    'Redux/StoryProgressRedux': 'INFO',
    'Sagas/FoodDiarySaga': 'INFO',
    'Sagas/GiftedChatMessageSaga': 'INFO',
    'Sagas/MessageSagas': 'INFO',
    'Sagas/ServerSyncSagas': 'INFO',
    'Utils/PushNotifications': 'INFO'
}
```

___

### Typing Indicators

**fastMode**  
If you active this, the typing speed will be skipped. The messages are instantly in the chat.
Also the wait-command will be skipped.

Default value: *false*

**coachTypingSpeed**  
Typing speed (words/minute)

Default value: *400*

**maxTypingDelay**  
Max delay for larger msgs (in ms)

Default value: *5000*

**interactiveElementDelay**  
Delay before active elements (in ms)

Default value: *300*
___

### Messages

**initialNumberOfMinimalShownMessages**  
Description

Default value: *10*

**incrementShownMessagesBy**  
Description

Default value: *25*

**showEmptyChatMessage**  
Show message instead if loading-indicator if chat is empty

Default value: *false*

**showAnswerExpiredMessage**  
true: expired answer-buttons are switched to an 'answer expired'-message  
false: expired answers are being greyed out

Default value: *false*

**showExpiryAlert**  
If true, alert text is displayed when user taps on expired message (text defined in i18n json)

Default value: *true*
___

### Startup

**automaticallyRequestPushPermissions**  
Description

Default value: *false*

**automaticallyConnectOnFirstStartup**  
Description

Default value: *true*

**automaticallyShareObserverAccessToken**  
Description

Default value: *false*

**automaticallyShareParticipantAccessToken**  
Description

Default value: *false*

**backButtonInOnboardingEnabled**  
Description

Default value: *false*

**onboardingURL**  
Description

Default value: *---*
___

### Server Synchronisation

**useLocalServer**  
Description

Default value: *false*

**userChatEnabled**  
Description

Default value: *true*

**dashboardChatEnabled**  
Description

Default value: *true*

**sendRecordedMediaLengthValues**  
Null or varible name if record length shall be automatically communicated to server

Default value: *lastRecordLength*

**clientVersion**  
Description

Default value: *1*

**role**  
Description

Default value: *participant*  
Possible values: *participant, supervisor, observer, team-manager*

**defaultNickname**  
Description

Default value: *MobileCoach Client User*

**interventionPattern**  
Name of the intervention you want to connect the client.

Default value: *-*

**interventionPassword**  
Password of the intervention you want to connect the client.

Default value: *-*

**androidSenderId**  
Put in here the sender-id of Firebase Cloud Messaging

Default value: *1234567890*

**localDeepstreamURL**  
Description

Default value: *ws://127.0.0.1:8082/deepstream*

**localRestURL**  
Description

Default value: *http://127.0.0.1:8080/PMCP/api/v02/*

**localMediaURL**  
Description

Default value: *http://127.0.0.1:8080/PMCP/files/*

**remoteDeepstreamURL**  
Replace the "---" with the hostname of your server.

Default value: *wss://---/deepstream*

**remoteRestURL**  
Replace the "---" with the hostname of your server.

Default value: *https://---/PMCP/api/v02/*

**remoteMediaURL**  
Replace the "---" with the hostname of your server.

Default value: *https://---/PMCP/files/*

**mediaUploadsWithAuthenticiation**  
Whether or not user uploads are protected with auth-tokens

Default value: *true*
___
