---
id: setup
title: Setting up the mobile app
---

## **Requirements**

***

* Administrator permissions
* Installed [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Please follow the [React Native CLI Quickstart Guide](https://reactnative.dev/docs/environment-setup) for your desired development/target OS. If you do that correctly, you're ready to deploy this repository.

## **Configuration**

***

### Cloning the repository

```
git clone https://bitbucket.org/mobilecoach/mobilecoach-mobile-app.git
cd mobilecoach-mobile-app
```

### Setting the config file

* First open the config file (Path: *App/Config/AppConfig.js*) in an editor.
* Change the intervention variables to the information of the created or existing intervention.
* Change the remote URL variables to the host you want to connect.

```javascript
interventionPattern: '[INTERVENTION-NAME]', // name of the intervention you want to connect
interventionPassword: '[DEEPSTREAM-CLIENT-PASSWORD]', // also in the intervention dashboard

remoteDeepstreamURL: 'wss://[HOSTNAME]/deepstream',
remoteRestURL: 'https://[HOSTNAME]/PMCP/api/v02/',
remoteMediaURL: 'https://[HOSTNAME]/PMCP/files/',
```

**For example:**

```javascript
interventionPattern: 'WORKSHOP',
interventionPassword: 'workshop',
remoteDeepstreamURL: 'wss://workshop-cdhi.ethz.ch/deepstream',
remoteRestURL: 'https://workshop-cdhi.ethz.ch/PMCP/api/v02/',
remoteMediaURL: 'https://workshop-cdhi.ethz.ch/PMCP/files/',
```

All other variables are explained [HERE](../mobileapp/configs.md)

### Changing the project name and the App identifier

:::note
This needs to be changed if you want to use push notifications and/or publish the app.
:::

First you have to open your mobilecoach-client folder in the terminal.
You can simply rename almost all of the needed files.

```
react-native-rename "[NEW-PROJECT-NAME]" -b [NEW-APP-IDENTIFIER]
watchman watch-del-all
```

For example:

```
react-native-rename "WorkshopClient" -b ch.ethz.workshop.test.client
watchman watch-del-all
```

**Additional Todo for Android:**  
Open the google-services file in (Path: *android/app/google-services.json*).
Edit the package name on line 13.

```json
"package_name": "[NEW-APP-IDENTIFIER]"
```

For example:

```json
"package_name": "ch.ethz.workshop.test.client"
```

:::caution
react-native-rename works for MacOS users and it may not work for other platforms. If you are using Windows or Linux, please try [this instruction.](https://github.com/facebook/react-native/issues/24616#issuecomment-594324874)
:::

### Installing all dependencies

You have to be in the mobilecoach-client folder.

```
yarn install
```

```
cd ios/ && pod install
```
```
cd ..
```

### Starting the metro bundler

In a new tab of your terminal, run the following command in the mobilecoach-client folder. This should start the metro bundler.
```
yarn start
```

### Running the app

:::note
Important: A connected smartphone or an installed emulator is required.
:::

```
yarn android
```

OR running it for instance on an iPhone Simulator

```
yarn ios
```

## **Troubleshooting**

***

* Presence of a ```.git``` file in the project folders in node_modules would produce an error when executing ```yarn``` commands. Please remove them as shown below.

```
cd node_modules/react-native-push-notification
rm -r .git/
cd ..
cd react-native-restart
rm -r .git/
```

* Building failed because of "java.io.IOException: Unable to delete directory ```pathToPackage```"
Execute the following commands:

```
cd android
gradlew clean
cd ..
```
