---
id: designchanges
title: Change design / coaches
---

## Overview

There are several ways how to change the design of the apps. We show you the easiest ones here.

## Change colors/images in Brand.js

You can find the file here: App -> Themes -> Brand.js
This file is like a top-level configuration of your app colors.

:::note
The brand.js file has to be revised, it's not logical/self-explanatory anymore 😅...
:::

Description of all color codes:

```javascript title="App/Themes/Brand.js"
const brand = {
  colors: {
    // Main color (onboarding, header, icons)
    primary: '#2c3e50',

    // not used
    primaryText: '#fff',

    // chat - active answer bubbles colors
    buttonBackground: '#3498db',

    // Main text color (onboarding, active user bubbles)
    buttonText: '#fff',

    // not used
    backgroundMain: '#fff',

    // text color in sidemenu, faq, settings
    textMain: '#3c3c3c',

    // background color of the chatbot bubbles
    background1: '#f7f7f7',

    // text color in the chatbot bubbles
    text1: '#2c3e50',

    // background color of the non-active (chat history) user bubbles
    background2: '#2c3e50',

    // text color in the non-active (chat-history) user bubbles
    text2: '#fff',

    // title of infocards/medialibrary
    grey1: '#525252',

    // subtitle in infocard/medialibrary, date/time on top of chat
    grey2: '#9B9B9B',

    // background color in infocards, medialibrary, faq, settings
    grey3: '#F8F8F8'
  },
  images: {
    // used in startScreen (onboarding) and on top of the sidemenu
    logo: require('../Images/app_logo.png'),

    // used in the startScreen (onboarding) and at the bottom of the sidemenu
    poweredBy: require('../Images/powered_by.png'),

    // chat background image, should be repetitive. Used in both chats as background.
    chatBackground: require('../Images/Chat/bg.png')
  }
}
```

In the file 'Colors.js' you'll find the colors in more detail.

## Changing coaches

### Changing name

To change the name, you have to go in the language files you use. Adapt the following lines:

```json title="App/I18n/languages/english.json"
  "Coaches": {
    "0": "Lukas",
    "1": "Anna"
  },
```

### Changing avatar image

The coach avatars needs to be in this format:

- minumum of 300x300 pixel
- .png format
- round cut out and white background

You can make a avatar with several tools like this [Avatarmaker](https://avatarmaker.com/)

Put the images in the App -> Images folder and link them in the Images.js file.

```javascript title="App/Themes/Images.js"
  coaches: [
    require('../Images/Coaches/coach_male.png'),
    require('../Images/Coaches/coach_female.png')
  ],
```
