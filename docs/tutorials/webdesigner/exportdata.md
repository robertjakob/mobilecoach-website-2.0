---
id: exportdata
title: Exporting data
---

## Exporting participant data

Go to the intervention list **'Interventions' menu item**, select your intervention and click on the **Results** button. This opens the following window:

![Exporting Data](/img/exporting-data.png 'Exporting Data')

Here you can see all participants and their data/variables. With a click on **Refresh**, the list of all participants and their data will be reloaded. If you click on **Send Message** and the intervention is running, you can manually send messages to the participants. This is mostly used for debugging/testing purposes.

If you want to download the data, you can select all participant and click the **Export All Data** button. You can also select only a few participants and download their data. You will get an .CSV file that is semi-colon separated.

You can also export the variables of the selected participants in the second table. The export format is also .CSV, but each variable will use one row. You can also download the message dialogs of the selected participants in the last table.
