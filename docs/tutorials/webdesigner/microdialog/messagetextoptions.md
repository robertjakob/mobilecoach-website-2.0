---
id: messagetext
title: Message text options
---

Here you can add the text, that you want to have in your message. If multiple languages are available, it’s separated with buttons (e.g. English and German).

![Triple Hyphen](/img/text-hyphen.png 'Triple Hyphen')

## Add a variable in the text

You can either write you variable name (with $) in the text or just click on the variable you want to show on the variable list at the right. In the chat text, there will be the variable value. For instance you can use the $coach variable, after you created it.

```
“Hi I’m $coach, nice to meet you.”
```

## Triple-hyphen-technique (---)

You can split your message into different chat bubbles with the triple-hyphen-technique. Just add '---' between two texts as you can see in the image below. For example:

```
“Hi!”
---
“It's great that you chose me, respectively this app.”
```

## Add Emojis

There are several ways to add Emojis in your chat message. You can copy & paste it from a website or you can use the shortcuts to activate the emoji-keyboard.

Windows: windows + . (dot)  
macOS: control + command + space
