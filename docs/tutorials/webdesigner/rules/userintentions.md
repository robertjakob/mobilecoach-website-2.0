---
id: userintentions
title: User intentions
---

User intentions are sent from the mobile app. These intents can be catched and reacted to.
There exist several user intentions, only the 'go' intention is kind of mandatory.

In general you can add user intention rules in 'Rules' --> 'Execution on USER INTENTION'. To add a user intention you need these two variables:

* $participantIntention (intention name like 'go', 'platform' etc.)
* $participantIntentionContent (intention content like 'ios' or 'android')

### All intentions explained

| Intention | Sent value | Explanation |
|-------|-------|-------|
| go | no value | This will be sent when the participant clicks on 'Let's go!' after the coach selection. You need to catch this to start your intervention/micro-dialogs. No value is sent with it. |
| coach | selected coach name | This will be sent after the participant selected a coach. |
| platform | ios or android | This will be sent directly when the participant opens the app. There are for now two possible values: “ios” and “android”. |
| language | selected language | This will be sent after the participant selected a language. |
| access-token-observer | access token observer | It is needed for the web cockpit. |
| access-token-participant | access token participant | It is needed for the web cockpit. |
| web-completed | no value | This will close the webview. It is used for Limesurvey integration. |

### Example of implemented intention rules

![Intentions](/img/intention-all.png 'Intentions')

### Example of 'go' intention

![Intention GO](/img/intention-go.png 'Intention GO')

### Example of 'platform' intention

![Intention Platform 1](/img/intention-platform-1.png 'Intention Platform 1')

![Intention Platform 2](/img/intention-platform-2.png 'Intention Platform 2')
