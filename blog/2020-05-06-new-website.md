---
title: New MobileCoach website
author: Sascha Gfeller
author_title: MobileCoach Team
author_url: https://www.c4dhi.org/team/
author_image_url: https://www.c4dhi.org/wp-content/uploads/2019/08/sascha-gfeller-full.jpg
tags: [mobilecoach, website, documentation]
---

We are currently developing the new MobileCoach website. The focus is on documentation, which is why we decided to use the framework Docusaurus. It is our goal that the documentation not only contains technical elements (e.g. setting up the server), but also instructions for creating a digital intervention with MobileCoach itself.

This is the first version of the new website. We will continue to develop the website and expand the documentation. The repository for the website can be found here: [repository](https://bitbucket.org/mobilecoach/mobilecoach-website-2.0/src/master/). So you can also contribute to the documentation/website if you want to. Otherwise, you are welcome to send input for improving the website itself and/or the documentation to the following mail: sgfeller@ethz.ch

I wish you a nice day and a lot of fun with the new website.

Best regards,  
Sascha
